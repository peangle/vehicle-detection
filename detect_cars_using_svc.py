import glob
import time

import cv2
import matplotlib.image as mpimg
import numpy as np
from moviepy.editor import VideoFileClip
from scipy.ndimage.measurements import label
from skimage.feature import hog
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC

from detect_car_utils import slide_window, add_heat, apply_threshold, get_labeled_bboxes, draw_boxes, find_matches, \
    get_windows_to_search_cars_in


def get_hog_features(img, orient, pix_per_cell, cell_per_block,
                     vis=False, feature_vec=True):
    # Call with two outputs if vis==True
    if vis == True:
        features, hog_image = hog(img, orientations=orient, pixels_per_cell=(pix_per_cell, pix_per_cell),
                                  cells_per_block=(cell_per_block, cell_per_block), transform_sqrt=True,
                                  visualise=vis, feature_vector=feature_vec)
        return features, hog_image
    # Otherwise call with one output
    else:
        features = hog(img, orientations=orient, pixels_per_cell=(pix_per_cell, pix_per_cell),
                       cells_per_block=(cell_per_block, cell_per_block), transform_sqrt=True,
                       visualise=vis, feature_vector=feature_vec)
        return features


# Define a function to extract features from a list of images
# Have this function call bin_spatial() and color_hist()
def extract_features(image):
    cspace = 'YUV'
    orient = 15
    pix_per_cell = 2
    cell_per_block = 1
    hog_channel = 'ALL'

    # apply color conversion if other than 'RGB'
    if cspace != 'RGB':
        if cspace == 'HSV':
            feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        elif cspace == 'LUV':
            feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2LUV)
        elif cspace == 'HLS':
            feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
        elif cspace == 'YUV':
            feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2YUV)
        elif cspace == 'YCrCb':
            feature_image = cv2.cvtColor(image, cv2.COLOR_RGB2YCrCb)
        else:
            feature_image = np.copy(image)
    else:
        feature_image = np.copy(image)

    # Call get_hog_features() with vis=False, feature_vec=True
    if hog_channel == 'ALL':
        hog_features = []
        for channel in range(feature_image.shape[2]):
            hog_features.append(get_hog_features(feature_image[:, :, channel],
                                                 orient, pix_per_cell, cell_per_block,
                                                 vis=False, feature_vec=True))
        hog_features = np.ravel(hog_features)
    else:
        hog_features = get_hog_features(feature_image[:, :, hog_channel], orient,
                                        pix_per_cell, cell_per_block, vis=False, feature_vec=True)

    # Return list of feature vectors

    return np.append(hog_features, feature_image)


def train_using_hog_svc(sample_size=None):
    # Divide up into cars and notcars
    images = glob.glob('./data/non-vehicles/**/*.png')
    notcars = []
    for image in images:
        notcars.append(image)

    images = glob.glob('./data/vehicles/**/*.png')
    cars = []
    for image in images:
        cars.append(image)

    if sample_size:
        cars = cars[0:sample_size]
        notcars = notcars[0:sample_size]

    t = time.time()
    car_features = []
    for file in cars:
        image = mpimg.imread(file)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        car_features.append(extract_features(image))

    notcar_features = []
    for file in notcars:
        image = mpimg.imread(file)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        notcar_features.append(extract_features(image))

    t2 = time.time()
    print(round(t2 - t, 2), 'Seconds to extract HOG features...')
    # Create an array stack of feature vectors
    X = np.vstack((car_features, notcar_features)).astype(np.float64)
    # Fit a per-column scaler
    X_scaler = StandardScaler().fit(X)
    # Apply the scaler to X
    scaled_X = X_scaler.transform(X)

    # Define the labels vector
    y = np.hstack((np.ones(len(car_features)), np.zeros(len(notcar_features))))

    # Split up data into randomized training and test sets
    rand_state = np.random.randint(0, 100)
    X_train, X_test, y_train, y_test = train_test_split(
        scaled_X, y, test_size=0.2, random_state=rand_state)

    print('Feature vector length:', len(X_train[0]))
    # Use a linear SVC
    svc = LinearSVC()
    # Check the training time for the SVC
    t = time.time()
    svc.fit(X_train, y_train)
    t2 = time.time()
    print(round(t2 - t, 2), 'Seconds to train SVC...')
    # Check the score of the SVC
    print('Test Accuracy of SVC = ', round(svc.score(X_test, y_test), 4))
    # Check the prediction time for a single sample
    t = time.time()
    n_predict = 10
    print('My SVC predicts: ', svc.predict(X_test[0:n_predict]))
    print('For these', n_predict, 'labels: ', y_test[0:n_predict])
    t2 = time.time()
    print(round(t2 - t, 5), 'Seconds to predict', n_predict, 'labels with SVC')
    return svc


images = []
previous_frames_with_cars_bbox = []


def mark_cars_using_svc(svc, image):
    global images, previous_frames_with_cars_bbox

    windows = get_windows_to_search_cars_in(image)
    cars = []
    for window in windows:
        cropped_image = image[window[0][1]:window[1][1], window[0][0]:window[1][0]]
        cropped_image = cv2.resize(cropped_image, (64, 64))
        prediction = svc.predict([extract_features(cropped_image)])
        if prediction[0] == 1:
            cars.append(window)

    prev_cars_bbox = []
    [prev_cars_bbox.extend(find_matches(image, bbox)) for bbox in previous_frames_with_cars_bbox]

    cars.extend(prev_cars_bbox)

    heat = np.zeros_like(image[:, :, 0]).astype(np.float)
    heat = add_heat(heat, cars)
    heat = apply_threshold(heat, 4 if len(prev_cars_bbox) > 0 else 2)
    heatmap = np.clip(heat, 0, 255)
    labels = label(heatmap)

    bboxes = get_labeled_bboxes(image, labels)

    if len(previous_frames_with_cars_bbox) > 2:
        previous_frames_with_cars_bbox.pop()

    previous_frames_with_cars_bbox.append(bboxes)

    return draw_boxes(image, bboxes), heatmap


def process_image(image):
    global svc
    img_with_cars, heatmap = mark_cars_using_svc(svc, image)
    return img_with_cars


def process_project_video(svc):
    challenge_output = './cropped_processed.mp4'
    clip2 = VideoFileClip('./cropped.mp4')
    challenge_clip = clip2.fl_image(process_image)
    challenge_clip.write_videofile(challenge_output, audio=False)
