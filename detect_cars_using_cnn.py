import glob
import pickle

import cv2
import numpy as np
from keras.layers.convolutional import Convolution2D
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.pooling import AveragePooling2D
from keras.models import Sequential
from keras.models import model_from_json
from moviepy.editor import VideoFileClip
from scipy.ndimage.measurements import label
from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle

from detect_car_utils import draw_boxes, get_labeled_bboxes, apply_threshold, add_heat, slide_window, find_matches, \
    get_windows_to_search_cars_in


def get_keras_model():
    model = Sequential()
    model.add(Convolution2D(16, 13, 13, border_mode='same', input_shape=(64, 64, 3)))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 32x32x16
    model.add(Activation('relu'))

    model.add(Convolution2D(32, 7, 7, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 16x16x32
    model.add(Activation('relu'))

    model.add(Convolution2D(64, 5, 5, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 8x8x64
    model.add(Activation('relu'))

    model.add(Convolution2D(128, 3, 3, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 4x4x128
    model.add(Activation('relu'))

    model.add(Convolution2D(256, 3, 3, border_mode='same'))
    model.add(AveragePooling2D((2, 2), border_mode='same'))  # 2x2x256
    model.add(Activation('relu'))

    model.add(Flatten())
    model.add(Dense(500))
    model.add(Dropout(0.5))
    model.add(Activation('relu'))

    model.add(Dense(100))
    model.add(Activation('relu'))

    model.add(Dense(10))
    model.add(Activation('relu'))

    model.add(Dense(1))
    model.add(Activation('sigmoid'))

    return model


def pickle_car_data():
    images = []
    files = []
    for filename in glob.iglob('./data/vehicles/**/*.png'):
        files.append(filename)
    for filename in files:
        print(filename)
        image = cv2.resize(cv2.imread(filename), (64, 64))
        images.append(image)

    with open("./car_data.p", 'wb') as output_file:
        pickle.dump(images, output_file)


def pickle_non_car_data():
    images = []
    files = []
    for filename in glob.iglob('./data/non-vehicles/**/*.png'):
        files.append(filename)
    for filename in files:
        print(filename)
        image = cv2.resize(cv2.imread(filename), (64, 64))
        images.append(image)

    with open("./non_car_data.p", 'wb') as output_file:
        pickle.dump(images, output_file)


def load_train_data(bgr_to_x):
    with open("./car_data.p", mode='rb') as f:
        car_images = pickle.load(f)
    car_features = car_images
    car_features = [cv2.cvtColor(image, bgr_to_x) for image in car_features]

    car_labels = np.ones(len(car_images))

    with open("./non_car_data.p", mode='rb') as f:
        non_car_images = pickle.load(f)
    if len(non_car_images) > len(car_images):
        non_car_images = non_car_images[:len(car_images)]
    non_car_features = non_car_images
    non_car_features = [cv2.cvtColor(image, bgr_to_x) for image in non_car_features]

    non_car_labels = np.zeros(len(non_car_images))
    labels = np.append(car_labels, non_car_labels)
    images = car_features + non_car_features

    return np.array([normalize_data(image) for image in images]), labels


def normalize_data(data):
    std = np.std(data)
    if std == 0:
        return np.float32((data[:] - np.mean(data)))
    return np.float32((data[:] - np.mean(data)) / std)


def train_using_keras(bgr_to_x):
    train_data, train_labels = load_train_data(bgr_to_x)
    print("Data loaded...")
    train_data, train_labels = shuffle(train_data, train_labels)
    one_hot_labels = LabelEncoder().fit_transform(train_labels)
    print("Creating model...")
    model = get_keras_model()
    print("Training...")
    model.compile('adam', 'binary_crossentropy', ['accuracy'])
    model.fit(train_data, one_hot_labels, batch_size=32,
              nb_epoch=5, validation_split=0.2)

    model.save_weights('./model.h5')
    with open("./model.json", "w") as file:
        file.write(model.to_json())
    print("Model saved.")


images = []
previous_frames_with_cars_bbox = []
def mark_cars_using_cnn(model, image, image_features):
    global images, previous_frames_with_cars_bbox

    windows = get_windows_to_search_cars_in(image)
    cars = []
    for window in windows:
        cropped_image = image_features[window[0][1]:window[1][1], window[0][0]:window[1][0]]
        features = normalize_data(cv2.resize(cropped_image, (64, 64)))
        array = np.array([features])
        prediction = model.predict(array, batch_size=1)
        if prediction > 0.8:
            cars.append(window)

    prev_cars_bbox = []
    [prev_cars_bbox.extend(find_matches(image, bbox)) for bbox in previous_frames_with_cars_bbox]

    cars.extend(prev_cars_bbox)

    heat = np.zeros_like(image[:, :, 0]).astype(np.float)
    heat = add_heat(heat, cars)
    heat = apply_threshold(heat, 4 if len(prev_cars_bbox) > 0 else 2)
    heatmap = np.clip(heat, 0, 255)
    labels = label(heatmap)

    bboxes = get_labeled_bboxes(image, labels)

    if len(previous_frames_with_cars_bbox) > 2:
        previous_frames_with_cars_bbox.pop()

    previous_frames_with_cars_bbox.append(bboxes)

    return draw_boxes(image, bboxes), heatmap


with open('./model.json', 'r') as jfile:
    model = model_from_json(jfile.read())
    model.compile('adam', 'binary_crossentropy', ['accuracy'])
    model.load_weights("./model.h5")


def process_image(image):
    global model
    img_with_cars, heatmap = mark_cars_using_cnn(model, image, cv2.cvtColor(image, cv2.COLOR_RGB2YUV))
    return img_with_cars


def process_project_video():
    challenge_output = './project_video_processed.mp4'
    clip2 = VideoFileClip('./project_video.mp4')
    challenge_clip = clip2.fl_image(process_image)
    challenge_clip.write_videofile(challenge_output, audio=False)


#pickle_car_data()
#pickle_non_car_data()
#train_using_keras(cv2.COLOR_BGR2YUV)
#process_project_video()
