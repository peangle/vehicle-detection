import glob
import pickle
import time

import cv2
import numpy as np
from keras.models import Sequential
from keras.models import model_from_json
from moviepy.editor import VideoFileClip
from skimage.feature import hog


def current_millis():
    """
    :return: Current time in milli seconds
    """
    return int(round(time.time() * 1000))


def draw_boxes(img, bboxes, color=(0, 0, 255), thick=6):
    """
    Draws provided rectangle boxes onto the image
    :param img: Image to draw boxes on
    :param bboxes: List of tuple of top, left and bottom, right defining the bounding boxes
    :param color:
    :param thick:
    :return:
    """
    # Make a copy of the image
    imcopy = np.copy(img)
    # Iterate through the bounding boxes
    for bbox in bboxes:
        # Draw a rectangle given bbox coordinates
        cv2.rectangle(imcopy, bbox[0], bbox[1], color, thick)
    # Return the image copy with boxes drawn
    return imcopy


def slide_window(img, x_start_stop=[None, None], y_start_stop=[None, None],
                 xy_window=(64, 64), xy_overlap=(0.5, 0.5)):
    # If x and/or y start/stop positions not defined, set to image size
    if x_start_stop[0] == None:
        x_start_stop[0] = 0
    if x_start_stop[1] == None:
        x_start_stop[1] = img.shape[1]
    if y_start_stop[0] == None:
        y_start_stop[0] = 0
    if y_start_stop[1] == None:
        y_start_stop[1] = img.shape[0]
    # Compute the span of the region to be searched
    xspan = x_start_stop[1] - x_start_stop[0]
    yspan = y_start_stop[1] - y_start_stop[0]
    # Compute the number of pixels per step in x/y
    nx_pix_per_step = np.int(xy_window[0] * (1 - xy_overlap[0]))
    ny_pix_per_step = np.int(xy_window[1] * (1 - xy_overlap[1]))
    # Compute the number of windows in x/y
    nx_buffer = np.int(xy_window[0] * (xy_overlap[0]))
    ny_buffer = np.int(xy_window[1] * (xy_overlap[1]))
    nx_windows = np.int((xspan - nx_buffer) / nx_pix_per_step)
    ny_windows = np.int((yspan - nx_buffer) / ny_pix_per_step)
    # Initialize a list to append window positions to
    window_list = []
    # Loop through finding x and y window positions
    # Note: you could vectorize this step, but in practice
    # you'll be considering windows one by one with your
    # classifier, so looping makes sense
    for ys in range(ny_windows):
        for xs in range(nx_windows):
            # Calculate window position
            startx = xs * nx_pix_per_step + x_start_stop[0]
            endx = startx + xy_window[0]
            starty = ys * ny_pix_per_step + y_start_stop[0]
            endy = starty + xy_window[1]
            # Append window position to list
            window_list.append(((startx, starty), (endx, endy)))
    # Return the list of windows
    return window_list


def add_heat(heatmap, bbox_list):
    # Iterate through list of bboxes
    for box in bbox_list:
        # Add += 1 for all pixels inside each bbox
        # Assuming each "box" takes the form ((x1, y1), (x2, y2))
        heatmap[box[0][1]:box[1][1], box[0][0]:box[1][0]] += 1

    # Return updated heatmap
    return heatmap


def apply_threshold(heatmap, threshold):
    # Zero out pixels below the threshold
    heatmap[heatmap <= threshold] = 0
    # Return thresholded map
    return heatmap


def get_labeled_bboxes(img, labels):
    bboxes = []
    # Iterate through all detected cars
    for car_number in range(1, labels[1] + 1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        bbox = ((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy)))
        width = bbox[1][0] - bbox[0][0]
        height = bbox[1][1] - bbox[0][1]

        if height > width:
            continue

        center = bbox[0][0] + width//2, bbox[0][1] + height//2
        max_width = img.shape[1] * 15 // 100
        max_height = img.shape[0] * 15 // 100
        if width > max_width:
            bbox = ((center[0] - (max_width//2), bbox[0][1]), (center[0] + (max_width // 2), bbox[1][1]))
        if height > max_height:
            bbox = ((bbox[0][0], center[1] - (max_height // 2)), (bbox[1][0], center[1] + (max_height // 2)))

        bboxes.append(bbox)
    # Return the image
    return bboxes


def find_matches(img, template_list):
    """
    Find given templates in the given image.
    :param img: Image to find templates in
    :param template_list: List of bounding boxes in image defining templates
    :return: List of bounding boxes in image matching the templates
    """
    # Define an empty list to take bbox coords
    bbox_list = []
    # Define matching method
    # Other options include: cv2.TM_CCORR_NORMED', 'cv2.TM_CCOEFF', 'cv2.TM_CCORR',
    #         'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED'
    method = cv2.TM_CCOEFF_NORMED
    # Iterate through template list
    for bbox in template_list:
        # Read in templates one by one
        tmp = img[bbox[0][1]:bbox[1][1], bbox[0][0]:bbox[1][0]]
        # Use cv2.matchTemplate() to search the image
        result = cv2.matchTemplate(img, tmp, method)
        # Use cv2.minMaxLoc() to extract the location of the best match
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
        # Determine a bounding box for the match
        w, h = (tmp.shape[1], tmp.shape[0])
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        # Append bbox position to list
        bbox_list.append((top_left, bottom_right))
        # Return the list of bounding boxes

    return bbox_list


def get_windows_to_search_cars_in(image):
    """
    Returns the windows for different sizes to look for a car in image
    :param image:
    :return:
    """
    windows = []
    height = image.shape[0]
    width = image.shape[1]
    min_val = min(height, width)
    window_vals = [
        ((min_val * 5 // 100, min_val * 5 // 100), ((height * 55 // 100), (height * 60 // 100))),
        ((min_val * 10 // 100, min_val * 10 // 100), ((height * 55 // 100), (height * 80 // 100))),
        ((min_val * 15 // 100, min_val * 15 // 100), ((height * 55 // 100), (height * 85 // 100))),
        ((min_val * 20 // 100, min_val * 20 // 100), ((height * 65 // 100), (height * 85 // 100))),
        ((min_val * 25 // 100, min_val * 25 // 100), ((height * 60 // 100), (height * 85 // 100)))]

    for window_val in window_vals:
        windows.extend(slide_window(image, x_start_stop=[0, width],
                                    y_start_stop=window_val[1],
                                    xy_window=window_val[0], xy_overlap=(0.65, 0.65)))

    return windows
